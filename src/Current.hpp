#pragma once

#include <cstdint>
#include <vector>

#include <QObject>
#include <QAudioDecoder>

class Current final: public QObject
{
	Q_OBJECT
public:
	explicit Current(QString const& filename,
	                 QObject* const parent = nullptr);

	

	auto duration() const { return duration_; }
	auto finished() const { return finished_; }

	void start();
private Q_SLOTS:

	void onError(QAudioDecoder::Error);
	void onFinish();
	void onBufferReady();

private:

	QAudioDecoder* const decoder;
	uint64_t duration_; ///< Milliseconds

	bool finished_;

	size_t nDecodedPackets = 0;
	size_t nSamples = 0;
};

